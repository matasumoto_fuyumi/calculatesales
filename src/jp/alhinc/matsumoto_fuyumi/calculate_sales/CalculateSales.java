package jp.alhinc.matsumoto_fuyumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class CalculateSales {
	public static void main(String[] args) {

		// 処理内容.1-1
		//1行単位でファイルからデータを読み取る初期値nullでセット
		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");

			// エラー処理.3-1-1(支店定義ファイルが存在しない場合)
			if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			// エラー処理.3-2-1(ファイルの中身が空の場合)
			if (file.length() == 0) {
				System.out.println("支店定義ファイルのフォーマットが不正です");
				return;
			}

			// 処理内容.1-2
			// 支店定義ファイルのマップ
			Map<String, String> codeAndName = new TreeMap<>();

			// 売上ファイル(コード,合計金額)のマップ
			Map<String, Long> salesTotal = new TreeMap<>();

			try {
				br = new BufferedReader(new FileReader(file));
				String line;

				// ()内が実行され、lineがnullでなければ実行
				while((line = br.readLine()) != null) {

					// branch.listをカンマで分割(bd = Branch definition(支店定義)の略称で使ってる)
					String[] bd = line.split(",");

					// エラー処理.3-1-2(支店定義ファイルのフォーマット不正)
					// ↓配列の要素数    ↓支店コードが3桁であるか  ↓支店名が空文字列か
					if(bd.length != 2 || !bd[0].matches("\\d{3}$") || bd[1].isEmpty()){
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;

					}

					// マップに追加(支店コードと支店名)
					codeAndName.put(bd[0], bd[1]);
				}

			} finally {
				br.close();
			}


			// 処理内容.2-1
			// Fileクラスのオブジェクトを生成する(読み込み対象ファイルの宣言)
			File dir = new File(args[0]);

			// listFilesメソッドを使用して一覧を取得する
			File[] files = dir.listFiles(new FilenameFilter() {

				@Override
				public boolean accept(File dir, String name) {
					return new File(dir, name).isFile() && name.matches("^\\d{8}.rcd$");
				}
			});

			Arrays.sort(files);

			// 一覧のnull・空チェック
			if(files != null) {

				//取得した一覧を表示する
				for (int i = 0; i < files.length; i++) {

					if (i != 0) {
						// 1つ目のファイル名
						int number = Integer.parseInt(files[i].getName().substring(0, 8));

						// 1つ前のファイル名
						int previousNumber = Integer.parseInt(files[i - 1].getName().substring(0, 8));

						// ファイルの連番の差が1じゃなかったらエラー処理
						if(number - previousNumber != 1){
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}
					}

					// ヒットしたファイルの読み込み
					BufferedReader br2 = null;

					try {
						br2 = new BufferedReader(new FileReader(files[i]));
						String code = br2.readLine(); // 1行目の支店コード
						String sale = br2.readLine(); // 2行目の売上金
						String error = br2.readLine(); // 3行目はエラー処理用

						// 売上ファイルの2行目に金額が入っていない場合に文字列「0」を入れる処理(nullでなければsaleを入れる)
						sale = sale == null ? "0" : sale;

						// エラー処理.3-2-3
						if(!codeAndName.containsKey(code)) {
							System.out.println(files[i].getName() + "の支店コードが不正です");
							return;
						}

						// エラー処理.3-2-4
						if(error != null) {
							System.out.println(files[i].getName() + "のフォーマットが不正です");
							return;
						}

						// sale = 2行目の売上金(文字列)を数値化にする
						long sales = Long.parseLong(sale);

						// 処理内容.2-2
						// 	マップのsalesTotalの中に指定したキーが存在するか確認を行う
						Long total = (salesTotal.containsKey(code) ? salesTotal.get(code) : 0L) + sales;

						// 合計金額が10桁を超えた場合のエラー処理.3-2-2
						if(total.toString().length() > 10) {
							System.out.println("予期せぬエラーが発生しました");
							return;
						}

						salesTotal.put(code, total);

					} finally {
						br2.close();
					}
				}

			}

			// 処理内容.3-1
			//ディレクトリの指定とファイル生成
			File fileOut = new File(args[0], "branch.out");
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileOut));

			try {
				// キー、値の組を取り出すSetを取り出す(salesTotalの場合)
				for(Entry<String, Long> entry : salesTotal.entrySet()){

					//↓支店コード(売上ファイル)      ↓支店名(支店定義ファイル)        ↓合計金額(売上ファイル)
					bw.write(entry.getKey() + "," + codeAndName.get(entry.getKey()) + "," + entry.getValue());
					bw.newLine();
				}

			} finally {
				bw.close();
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}

	}
}
